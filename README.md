# ASP.NET Web API Tutorial: Products

In the task, you will create a CRUD application using ASP.NET Core Web API. The task has a code of the sample web application with name [ProductsTutorialWeb](./ProductsTutorialWeb/). This tutorial will guide through the steps required to create a web application that supports various types of HTTP methods like GET, POST, PUT, and DELETE. CRUD is an acronym for [Create, Read, Update, and Delete](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete). It describes the four basic operations of persistent storage.

Before you start working on the task, [download and install the Postman app](https://www.postman.com/downloads), and [learn how to use this tool](https://learning.postman.com/docs/getting-started/introduction).


## Task Steps

### Create the Controller

Add the _ProductsController_ class to the [ProductsController.cs](ProductsTutorialWeb/Controllers/ProductsController.cs) file.

```cs
[ApiController]
[Route("[controller]")]
public sealed class ProductsController : ControllerBase
{
}
```

A web API [controller should derive from the ControllerBase class](https://docs.microsoft.com/en-us/aspnet/core/web-api#controllerbase-class) and it usually has the  _ApiController_ and _Route_ attributes.

The _ApiController_ attribute can be applied to a controller class to [enable API-specific behaviour](https://docs.microsoft.com/en-us/aspnet/core/web-api#apicontroller-attribute). Here it [makes attribute routing a requirement](https://docs.microsoft.com/en-us/aspnet/core/web-api#attribute-routing-requirement) with [[controller] token in route template](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/routing#token-replacement-in-route-templates-controller-action-area).



### Register Routing

Register routing in the [middleware](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/) pipeline in the [Startup.Configure](ProductsTutorialWeb/Startup.cs) method.

```cs
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    app.UseHttpsRedirection();

    app.UseRouting();
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
}
```

[Routing](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing#routing-basics) uses a pair of middleware registered by the _UseRouting_ and _UseEndpoints_ methods:

* _UseRouting_ adds route matching to the middleware pipeline. This middleware looks at the set of endpoints defined in the app and selects the best match based on the request.
* _UseEndpoints_ adds endpoint execution to the middleware pipeline. It runs the delegate associated with the selected endpoint.

The [MapControllers](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.builder.controllerendpointroutebuilderextensions.mapcontrollers) method adds endpoints for the controller actions to the endpoint route builder without specifying any routes.


### Check the Service Health

Add the _GetServiceHealth_ action to the _ProductsController_ class and apply _HttpGet_ attribute to the method.

```cs
[HttpGet("/healthz")]
public string GetServiceHealth()
{
    return "The service is healthy";
}
```

Run the application and open [this link](https://localhost:5001/healthz) in your browser.

![Products Test in Browser](images/products_test_in_browser.png)

Many browsers have a development toolset that can be used for inspecting network activity.

If you have Google Chrome, [open Chrome DevToos](https://developer.chrome.com/docs/devtools/open/) and [use the Network panel to view the network activity](https://developer.chrome.com/docs/devtools/network/) for the test web page.

![Products Test in Chrome DevTools](images/products_test_in_devtools.png)

The Microsoft Edge browser has [Edge DevTools](https://docs.microsoft.com/en-us/microsoft-edge/devtools-guide-chromium/open).

Also, it is possible to use Postman getting data from web applications.

![Products Test in postman](images/products_test_in_postman.png)

[Disable SSL Verification](https://learning.postman.com/docs/sending-requests/certificates/#certificate-data) if Postman display an error message.


### Create the Model

Add the _Product_ class to the [Product.cs](ProductsTutorialWeb/Models/Product.cs) file.

```cs
public class Product
{
    public string Id { get; set; }

    public string Name { get; set; }

    public double Price { get; set; }
}
```

The _Product_ class represents a product stored by the _ProductService_.


### Create the Service

Before adding a service, define a service interface in the [IProductsService.cs](ProductsTutorialWeb/Services/IProductService.cs) file.

```cs
public interface IProductService
{
    public List<Product> GetProducts();
}
```

Add the service to the [ProductsService.cs](ProductsTutorialWeb/Services/ProductService.cs) file.

```cs
public class ProductService : IProductService
{
    private readonly List<Product> productItems = new List<Product>();

    public List<Product> GetProducts()
    {
        return this.productItems;
    }
}
```

### Configure the Dependency Injection

The _IProductService_ interface will be used to configure the service as a [dependency injection](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection) for the _ProductsController_ class. Add a new parameter to the _ProductsController_ constructor.

```cs
[ApiController]
[Route("[controller]")]
public class ProductsController : ControllerBase
{
    private readonly IProductService productService;

    public ProductsController(IProductService productService)
    {
        this.productService = productService;
    }


    [HttpGet("/healthz")]
    public string GetServiceHealth()
    {
        return "The service is healthy";
    }
}
```

Register the dependency with _AddSingleton_ [service registration method](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection#service-registration-methods).

```cs
public void ConfigureServices(IServiceCollection services)
{
    services.AddControllers();
    services.AddSingleton<IProductService, ProductService>();
}
```

_AddSingleton_ enables [singleton service lifetime](https://docs.microsoft.com/en-us/dotnet/core/extensions/dependency-injection#singleton). That means the service is created when it is requested for the first time. Every subsequent request of the service implementation from the dependency injection container uses the same instance.



### Return the Service Data

Add a new action method with the name _GetProducts_ to the [ProductsController](ProductsTutorialWeb/Controllers/ProductsController.cs) class.

```cs
[HttpGet("/api/products")]
public ActionResult<List<Product>> GetProducts()
{
    return this.productService.GetProducts();
}
```

[HttpGet attribute](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.httpgetattribute) is used to identify an action that supports the HTTP GET method, and the attribute is used as [route template](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/routing#route-templates) here. Read more about [attribute routing with HTTP verb attributes](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/routing#route-templates).

Use the development toolset in your browser to get the product list.

![Empty Products in Chrome DevTools](images/empty_products_in_devtools.png)

Or use Postman.

![Empty Products in Postman](images/empty_products_in_postman.png)

The list should be empty because there are no products on the list.


### Add Product to the Service

Define a new method with the name _AddProduct_ in the _IProductService_ interface.

```cs
public Product AddProduct(Product productItem);
```

Then add the method implementation to the _ProductService_ class.

```cs
public Product AddProduct(Product productItem)
{
    this.productItems.Add(productItem);
    return productItem;
}
```

Add a new action to the _ProductController_ class with the _HttpPost_ attribute.

```cs
[HttpPost("/api/products")]
public ActionResult<Product> AddProduct(Product product)
{
    this.productService.AddProduct(product);
    return product;
}
```

[HttpPost attribute](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.httppostattribute) is used to identify an action that supports the HTTP POST method.

Use Postman to add a new product to the _ProductsTutorialWeb_ application. Add the "raw" body to the request.

```json
{ "id": "1", "name": "Coffee", "price": 3.04 }
```

![Add Product in Postman](images/add_product_in_postman.png)

Before sending the request, select the HTTP POST method in the dropdown, and add a new "Content-Type" header with the "application/json" value.

![Set Content-Type to application/json in Postman](images/set_content_type_application_json_in_postman.png)

Get the product list using Postman (or DevTools).

![Get Products in Postman](images/products_in_postman.png)


### Remove Product from the Service

Define a new method with the name _DeleteProduct_ in the _IProductService_ interface.

```cs
public string DeleteProduct(string id);
```

Then add the method implementation to the _ProductService_ class.

```cs
public string DeleteProduct(string id)
{
    int productIndex = this.productItems.FindIndex(p => string.CompareOrdinal(p.Id, id) == 0);
    if (productIndex >= 0)
    {
        this.productItems.RemoveAt(productIndex);
    }

    return id;
}
```

Add a new action to the _ProductController_ class with the _HttpDelete_ attribute.

```cs
[HttpDelete("/api/products/{id}")]
public ActionResult<string> DeleteProduct(string id)
{
    this.productService.DeleteProduct(id);
    return id;
}
```

Thw [HttpDelete attribute](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.httpdeleteattribute) is used to identify an action that supports the HTTP DELETE method.

Run the application, add a product as in the previous step, and use Postman to remove the recently added product.

![Remove Product in Postman](images/remove_product_in_postman.png)

Use Postman to get a product list and make sure the list is empty.


### Update Product Data

Define a new method with the name _UpdateProduct_ in the _IProductService_ interface.

```cs
public Product UpdateProduct(string id, Product productItem);
```

Then add the method implementation to the _ProductService_ class.

```cs
public Product UpdateProduct(string id, Product productItem)
{
    int productIndex = this.productItems.FindIndex(p => string.CompareOrdinal(p.Id, id) == 0);
    if (productIndex >= 0)
    {
        this.productItems[productIndex] = productItem;
    }

    return productItem;
}
```

Add a new action to the _ProductController_ class with the _HttpDelete_ attribute.

```cs
[HttpPut("/api/products/{id}")]
public ActionResult<Product> UpdateProduct(string id, Product product)
{
    this.productService.UpdateProduct(id, product);
    return product;
}
```

The [HttpPut attribute](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.httpputattribute) is used to identify an action that supports the HTTP PUT method.

Run application, add a product as in the previous step, and use Postman to update the recently added product. Add the "raw" body to the request.


```json
{ "id": "1", "name": "Tea", "price": 2.37 }
```

![Update Product in Postman](images/update_product_in_postman.png)


### Health Checks

ASP.NET Core has [Health Checks middleware](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks) for reporting the health of app infrastructure components.

Delete the _GetServiceHealth_ action from the _ProductsController_ controller class and add a health check endpoint by invoking the _MapHealthCheck_ method in _UseEndpoints_.

```cs
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    app.UseHttpsRedirection();
    app.UseRouting();
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapHealthChecks("/healthz");
        endpoints.MapControllers();
    });
}
```

Register a health check service in the _Startup.ConfigureServices_ method.

```cs
public void ConfigureServices(IServiceCollection services)
{
    services.AddHealthChecks();
    services.AddControllers();
    services.AddSingleton<IProductService, ProductService>();
}
```

Run the application and use Postman to check the health of the application.

![Checking App Health in Postman](images/health_check_in_postman.png)


### Error Handling

Define a new method with the name _GetProduct_ in the _IProductService_ interface.

```cs
public Product GetProduct(string id);
```

Then add the method implementation to the _ProductService_ class.

```cs
public Product GetProduct(string id)
{
    int productIndex = this.productItems.FindIndex(p => string.CompareOrdinal(p.Id, id) == 0);
    if (productIndex >= 0)
    {
        return this.productItems[productIndex];
    }

    throw new Exception($"Product with id='{id}' is not found.");
}
```

Add a new action to the _ProductController_ class with the _HttpGet_ attribute.

```cs
[HttpGet("/api/products/{id}")]
public ActionResult<Product> GetProduct(string id)
{
    return this.productService.GetProduct(id);
}
```

Run the application and use Postman to get the product details for the product that doesn't exist in the web application. The web application returns [Internal Server Error](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_server_errors) status code.

![Internal Server Error in Postman](images/get_product_internal_server_error.png)

Enable the [Developer Exception Page](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/error-handling#developer-exception-page) by invoking the [UseDeveloperExceptionPage method](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.builder.developerexceptionpageextensions.usedeveloperexceptionpage) for the [Development environment](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments).

```cs
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }

    app.UseHttpsRedirection();
    app.UseRouting();
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapHealthChecks("/healthz");
        endpoints.MapControllers();
    });
}
```

Run the application and use Postman to get the product details again.

![Get Error Details in Postman](images/get_product_error_details_in_postman.png)

If you use your browser to get the product details, you will get a styled error page.

![Get Error Details in Browser](images/get_product_error_details_in_browser.png)


Add exception handling to the _GetProduct_ method and return the action result by invoking the [ControllerBase.StatusCode method](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.controllerbase.statuscode) if an exception is thrown.

```cs
[HttpGet("/api/products/{id}")]
public ActionResult<Product> GetProduct(string id)
{
    try
    {
        return this.productService.GetProduct(id);
    }
    catch (Exception)
    {
        return this.StatusCode(404);
    }
}
```

Run the application and use Postman to get the product details again - the application will return [Not Found](https://en.wikipedia.org/wiki/HTTP_404) status code.

![Internal Server Error in Postman](images/get_product_not_found.png)


### Logging

Add a new constructor parameter to the _ProductsController_ class to [create a logger](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging#create-logs) that uses a log category of the fully qualified name of the type _ProductsController_.

```cs
private readonly ILogger logger;
private readonly IProductService productService;

public ProductsController(ILogger<ProductsController> logger, IProductService productService)
{
    this.logger = logger;
    this.productService = productService;
}
```

Call the _LogInformation_ and _LogError_ methods to log additional details about the _GetProduct_ method execution.

```cs
[HttpGet("/api/products/{id}")]
public ActionResult<Product> GetProduct(string id)
{
    this.logger.LogInformation($"GetProduct({id})");
    try
    {
        var product = this.productService.GetProduct(id);
        this.logger.LogInformation($"GetProduct({id}) returned a product.", product);
        return product;
    }
    catch (Exception e)
    {
        this.logger.LogError("Unable to get product information", e);
        return this.StatusCode(404);
    }
}
```

Run the application and use Postman to get the product details. After getting a response, switch to Visual Studio and open output pane.

![Log Error in Visual Studio](images/log_error_vs.png)

Add a product to the web application and use Postman to get the product details.

![Log Information in Visual Studio](images/log_information_vs.png)


## See also

* [ASP.NET Core Middleware](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware)
* [Routing Basics](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing)
* [Use multiple environments in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments)
* [Dependency Injection Basics](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection)
* [Handle errors in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/error-handling)
* [Tutorial: Create a web API with ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api)
* [Create web APIs with ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/web-api)
* [Routing to controller actions in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/routing)
* [Health checks in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks)
* [Dependency injection in .NET](https://docs.microsoft.com/en-us/dotnet/core/extensions/dependency-injection)
