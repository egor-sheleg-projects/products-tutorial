﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProductsTutorialWeb.Models;

namespace ProductsTutorialWeb.Services
{
    public interface IProductService
    {
        public List<Product> GetProducts();

        public Product AddProduct(Product productItem);

        public string DeleteProduct(string id);

        public Product UpdateProduct(string id, Product productItem);

        public Product GetProduct(string id);
    }
}
